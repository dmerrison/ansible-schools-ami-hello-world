Role Name
=========

Hello, World! in Ansible. 

Requirements
------------

Supports Ansible Molecule (`pip install molecule`) for testing in an Amazon
Linux 2 Docker container that supports systemd.

Role Variables
--------------

None.

Dependencies
------------

None.

